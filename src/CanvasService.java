import java.util.*;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import java.net.URI;
import java.net.URL;
import java.net.URISyntaxException;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.net.URLDecoder;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import oauth.signpost.basic.DefaultOAuthConsumer;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthCommunicationException;

// Used to parse HTML documents
import org.jsoup.*;
import org.jsoup.nodes.*;

@Path("/")
public class CanvasService {
    @Context ServletContext context;
    @Context HttpServletRequest request;
    
    private String oauthKey = "lti-codecheck-key";
    private String oauthSecret = "8MqVj7trPNCQej";
    
    @POST
    @Path("/choose-problem")
    @Produces("text/html")
    public Response chooseProblem(@FormParam("launch_presentation_return_url") String launchURL,
				  @FormParam("ext_content_return_types") String selectLink) {
	// This is how the assignment is going to be set up for a specific codecheck assignment
	String formPage = "<!DOCTYPE html>\n"
	    + "<html>\n"
	    + "  <head>\n"
	    + "    <title>Choose codecheck problem</title>\n"
	    + "    <script type=\"text/javascript\">\n"
	    + "      function modifyURL()\n"
	    + "      {\n"
	    + "          var protocol = window.location.protocol;\n"
	    + "          var host = window.location.host;\n"
	    + "          var ltiToolURL = protocol + \"//\" + host + \"/lti-codecheck/problem?url=\";\n"
	    + "          document.myform.url.value = ltiToolURL + encodeURIComponent(document.myform.url.value);\n"
	    + "          return true;\n"
	    + "      }\n"
	    + "    </script>\n"
	    + "      \n"
	    + "  </head>\n"
	    + "  <body>\n"
	    + "    <h1>Set up a codecheck problem</h1>\n"
	    + "    <p>Please put in the codecheck problem URL for this external tool assignment:</p>\n"
	    + "    <form method=\"get\" name=\"myform\" action=\"" + launchURL + "\" onsubmit=\"modifyURL();\">\n"
            + "      <input type=\"hidden\" name=\"return_type\" value=\"lti_launch_url\" />\n"
	    + "      <input style=\"display: block; width: 90%;\" type=\"text\" name=\"url\" value=\"\" placeholder=\"Codecheck URL\"/>\n"
	    + "      <input type=\"submit\" name=\"submit\" value=\"Submit\" />\n"
	    + "    </form>\n"
	    + "\n"
	    + "  </body>\n"
	    + "</html>\n";
	
	return Response.status(200).entity(formPage).build();
    }

    // ------------------------------------------------------------
    // Here's where the LTI_Example cloning work starts
    
    // This method is what Canvas redirects to when the student clicks on the
    // button from Canvas.
    @POST
    @Path("/problem")
    @Produces("text/html")
    /**
       This method will receive the POST request from Canvas, saving the
       necessary parameters from Canvas to use throughout this application
       session.

       @param personName the name of the user who is accessing the app
       @param gradePassbackURL the url to passback the grade to Canvas
       @param sourcedId the lis_result_sourcedid parameter (it's presence
                        indicates that this lti tool was launched as an Canvas
                        External Tool)
       @param personalEmail the email of the user who is accessing the app
       @return the Response to the page the user will see first
     */
    public Response postLTIParamsAndCookies(@FormParam("lis_person_name_full") String personName,
					    @FormParam("lis_outcome_service_url") String gradePassbackURL,
					    @FormParam("lis_result_sourcedid") String sourcedId,
					    @FormParam("lis_person_contact_email_primary") String personEmail,
					    @FormParam("launch_presentation_return_url") String returnURL,
					    @QueryParam("url") String codecheckURL)

	throws URISyntaxException, UnsupportedEncodingException {
	// first we have to verify the oauth signature, to make sure this isn't an
	// attempt to hack the planet
	// TODO: do something about OAuth here for verifying the nonce/signature
	    
	// make sure this is an assignment tool launch, not another type of
	// launch. Only assignment tools support the outcome service, since they
	// only appear in the Canvas gradebook.
	if (gradePassbackURL == null || sourcedId == null) {
	    String output = "It looks like this LTI tool wasn't launched "
                          + "as an assignment, or you are trying "
                          + "to take it as a teacher rather than as "
		          + "a student. Make sure to set up an "
		          + "external tool assignment as outlined for this example.";
	    return Response.status(200).entity(output).build();
	}

 	// store the relevant parameters from the launch into the user's
	// session, for access during subsequent http requests. note that the
	// name and email might be blank, if the tool wasn't configured in
	// Canvas provide that private information
	    
	NewCookie cookieGradePassbackURL = new NewCookie("lis_outcome_service_url", gradePassbackURL);
	NewCookie cookieSourcedId = new NewCookie("lis_result_sourcedid", sourcedId);
	NewCookie cookiePersonName = new NewCookie("lis_person_name_full", personName);
	NewCookie cookiePersonEmail = new NewCookie("lis_person_contact_email_primary", personEmail);
	NewCookie cookieReturnURL = new NewCookie("launch_presentation_return_url", returnURL);

	// That's it, setup is done. Now send them to the assessment! (Also, send the cookies to the browser)

	// Go to the codecheck problem
    	return Response.seeOther(new URI("/lti-codecheck/problem" + "?url=" + URLEncoder.encode(codecheckURL, "UTF-8")))
	    .cookie(cookieGradePassbackURL, cookieSourcedId, cookiePersonName, cookiePersonEmail, cookieReturnURL)
	    .build();
    }

    @GET
    @Path("/problem")
    @Produces("text/html")
    public Response getProblem(@QueryParam("url") String codecheckURL,
			       @CookieParam("lis_outcome_service_url") String gradePassbackURL) throws IOException {
	// This is the URL for the codecheck problem
	String grade = gradePassbackURL;

	// Get the html page
	URL url = new URL(codecheckURL);
	HttpURLConnection connection = (HttpURLConnection) url.openConnection();

	// get the text content of the page
        String text = this.getResponseBody(connection);

	// change the form action to work with this LTI application.
	// Instead of the default relative path (/codecheck/check), we
	// need to add more a specific relative path
	// (/lti-codecheck/codecheck/check)
	Document doc = Jsoup.parse(text);
	doc.select("form").attr("action", "/lti-codecheck/problem/codecheck/check");

	// the String of the html page content
	String htmlContent = doc.toString();

	return Response.status(200).entity(htmlContent).build();
    }

    @POST
    @Path("/problem/codecheck/check")
    @Produces("text/html")
    public Response checkProblem(MultivaluedMap<String, String> formParams,
				 @CookieParam("lis_outcome_service_url") String gradePassbackURL,
				 @CookieParam("lis_result_sourcedid") String sourcedId,
				 @CookieParam("launch_presentation_return_url") String returnURL,
				 @HeaderParam("referer") String referer) throws IOException, URISyntaxException, OAuthMessageSignerException, OAuthExpectationFailedException, OAuthCommunicationException, MalformedURLException {

	// ----- Get the form values
	String repoValue = "";
	String problemValue = "";
	String levelValue = "";
	String submitFileName = "";

	// We need to loop through the form parameters because the key
	// for the textArea code is based on the java file name.
	for (String key : formParams.keySet()) {
	    if (key.equals("repo")) { // get the repo value
		repoValue = formParams.getFirst(key);
	    } else if (key.equals("problem")) { // get the problem value
		problemValue = formParams.getFirst(key);
	    } else if (key.equals("level")) { // get the level value
		levelValue = formParams.getFirst(key); 
	    } else { // if none of the above, then the key is for the textarea. (get the submitFileName)
		submitFileName = key;
	    }
	}
	// -----

	// TODO: Make the post request relative; not only just to cs12.
	//       When I send the post request through this application to
	//       "http://go.code-check.org/codecheck/check", I get an
	//       IOException with the report file not being found
	/* The following code makes the post request relative, but the
	   above issue still needs to be resolved first
	   The issue is about the load balance. The AWS cookie needs to the sent manually to
	   http://go.code-check.org/codecheck/check
	*/
	String problemURLString = URLDecoder.decode(referer, "UTF-8");
	problemURLString = problemURLString.substring(problemURLString.indexOf("?url=") + "?url=".length());
	
	URL problemURL = new URL(problemURLString);
	String protocol = problemURL.getProtocol();
	String host = problemURL.getHost();
	int port = problemURL.getPort();
	String portString = (port > 0) ? ":" + port : ""; // make the port number appendable

	// the url to send the form to so that the actual codecheck can work its magic
	String urlString = protocol + "://" + host + portString + "/codecheck/check";

	// Here are the form parameters. In a regular map so that the
	// parameters can be sent to Minh's doPost method. (I don't
	// think it's worth it for me to modify it so the post can
	// work for MultivaluedMaps)
	Map<Object, Object> params = new HashMap<Object, Object>();
	params.put("repo", repoValue);
	params.put("problem", problemValue);
	params.put("level", levelValue);
	params.put(submitFileName, formParams.getFirst(submitFileName));

	// Send the form content to codecheck
	URLConnection connection = this.doPost(urlString, params);

	// Here's the URL for the report.html file from the form POST request
	// e.g., cs12.cs.sjsu.edu/codecheck/fetch/xxxxxx
	String reportURLPath = connection.getHeaderField("Location");
	String reportURLString = urlString.substring(0, urlString.lastIndexOf("/") + 1) + reportURLPath;
	List<String> codecheckCookies = connection.getHeaderFields().get("Set-Cookie");

	URL reportURL = new URL(reportURLString);
	HttpURLConnection reportConnection = (HttpURLConnection) reportURL.openConnection();
	if (codecheckCookies.size() > 1) {
	    // Two cookies to send: ccu cookie and AWSEB cookie (for go.code-check.org/codecheck/check)
	    String cookiesToSend = codecheckCookies.get(0) + codecheckCookies.get(1);
	    reportConnection.setRequestProperty("Cookie", cookiesToSend);
	}
	String reportText = this.getResponseBody(reportConnection);
	
	// Use a jsoup document so we can modify the download link href
	Document reportHtml = Jsoup.parse(reportText);

	// Here's the element with the download link. It's at the
	// bottom of the report page and the last link on the page, so
	// we'll get the "last <a> tag". (The download link is
	// actually the *only* link on the page, but let's get the
	// last one just to be safe.)
	Element downloadLink = reportHtml.select("a").last();

	// Here's the original href value that came with the page.
	// e.g., Numbers.signed.zip. We need this so we can change the
	// download link to be from cs12.[...] instead of being relative from
	// this LTI application. We don't have the file!
	String relativeDownloadPath = downloadLink.attr("href");

        // Suppose the following was the report link:
	// http://cs12.cs.sjsu.edu:8080/codecheck/fetch/14030711409047469045534875739/report.html
	// We don't actually want that last 'report.html' at the end
	// of the link. We need to replace it with the original href
	// file. For example, if the file we want to download from
	// this report is Numbers.signed.zip, then we want to have the link
	// look like:
	// http://cs12.cs.sjsu.edu:8080/codecheck/fetch/14030711409047469045534875739/Numbers.signed.zip

	// this is the URL without the 'report.html' part at the end
	String cs12URLPrefix = reportURLString.substring(0, reportURLString.lastIndexOf("/") + 1);

	// Here, we replace the download link with the absolute path to cs12.[...]/File.signed.zip
	reportHtml.select("p.download a").attr("href", cs12URLPrefix + relativeDownloadPath);


	// Let's get the score from the codecheck report so that we
	// can pass it back to the Canvas gradebook.
	Element scoreElement = reportHtml.select("p.score").last();

	// This is the raw score, e.g. 3/4, 4/4, 0, etc., from the codecheck report
	String scoreFraction = scoreElement.text();

	// This is the score, a double in the range [0.0,1.0], that will be sent with the grade passback url
	double scoreValue = CanvasService.normalizeScore(scoreFraction);

	// Pass back the grade to Canvas
	// This passes back the grade as well as the codecheck report URL
	String passbackXML = getScoreAndURLPassbackXML(sourcedId, scoreValue, reportURLString);
	URLConnection response = passbackGradeToCanvas(gradePassbackURL, passbackXML,
						       oauthKey, oauthSecret);

	response.getInputStream(); // This is needed, or else the grade won't get passed back.
                                   // It doesn't work when I simply call the connect() method.
	                           // But getInputStream() works. (Don't know why.)

	String output = reportHtml.toString();

	// Also, let's send the report.html back to Canvas as well.
	// TODO: check if this HTTPS stuff is going to be a problem
	// with different browsers
	
	// When using HTTPS, the content of the report file cannot be
	// seen directly because the codecheck page is HTTP (not HTTPS)	
	// return Response.seeOther(new URI(reportURLString)).build();
	// ^^^ The above Response will NOT show up.

	// Display the report page content.

	// In Chrome, the Download link doesn't work just by clicking
	// on it. An explicit "right-click > Save Link As..." has to
	// be done. My guess on why this happens is because the
	// content in the iframe is untrusted (because of the
	// self-signed certificate and/or the codecheck page is
	// delivered on HTTP)

	// On Firefox, the certificate isn't trusted (because of the
	// self-signed certificate used for testing purposes), so a jarring
	// security warning page shows up.
	
	// On Safari, there's a certificate warning that I can "Trust
	// Anyway", and the Download link can be clicked on normally
	// without any "Save Link As..." needed.
	
	// show the codecheck form on the page
	return Response.status(200).entity(output).build();
    }

    
    public static URLConnection doPost(String urlString, Map<Object, Object> nameValuePairs) throws IOException {
	// This is a modification of Minh Dang's original doPost
	// method. It returns a URLConnection instead of the response
	// body as a String

	// This is the URL to POST to
	URL url = new URL(urlString);

	// Connect to the URL
	HttpURLConnection connection = (HttpURLConnection) url.openConnection();

	// Set up connection for POST
	connection.setDoOutput(true);

	// Set the headers correctly
	connection.setRequestProperty("Accept-Charset", "UTF-8");
	connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

	// Don't automatically redirect links
	connection.setInstanceFollowRedirects(false);

	// Do POST
	try (PrintWriter out = new PrintWriter(connection.getOutputStream())) {

		// Write the POST parameters
		boolean first = true;
		for (Map.Entry<Object, Object> pair : nameValuePairs.entrySet()) {
		    if (first) first = false;
		    else out.print('&');
		
		    String name = pair.getKey().toString();
		    String value = pair.getValue().toString();
		    out.print(name);
		    out.print('=');
		    out.print(URLEncoder.encode(value, "UTF-8"));
		}
	    }

	return connection;
    }

    /**
     * Gets the passback XML to passback the score.
     * @param sourcedId the sourced ID from the LTI request. It
     *                  identifies the user and assignment to passback to.
     * @param score the score of the submission
     */
    public String getScorePassbackXML(String sourcedId, double score)
    {
	return "<?xml version = \"1.0\" encoding = \"UTF-8\"?>\n"
	    + "<imsx_POXEnvelopeRequest xmlns = \"http://www.imsglobal.org/lis/oms1p0/pox\">\n"
	    + "  <imsx_POXHeader>\n"
	    + "    <imsx_POXRequestHeaderInfo>\n"
	    + "      <imsx_version>V1.0</imsx_version>\n"
	    + "      <imsx_messageIdentifier>12341234</imsx_messageIdentifier>\n"
	    + "    </imsx_POXRequestHeaderInfo>\n"
	    + "  </imsx_POXHeader>\n"
	    + "  <imsx_POXBody>\n"
	    + "    <replaceResultRequest>\n"
	    + "      <resultRecord>\n"
	    + "        <sourcedGUID>\n"
	    + "          <sourcedId>" + sourcedId + "</sourcedId>\n"
	    + "        </sourcedGUID>\n"
	    + "        <result>\n"
	    + "          <resultScore>\n"
	    + "            <language>en</language>\n"
	    + "            <textString>" + score + "</textString>\n"
	    + "          </resultScore>\n"
	    + "        </result>\n"
	    + "      </resultRecord>\n"
	    + "    </replaceResultRequest>\n"
	    + "  </imsx_POXBody>\n"
	    + "</imsx_POXEnvelopeRequest>\n";
    }

    /**
     * Gets the passback XML to passback the score and the report url.
     * @param sourcedId the sourced ID from the LTI request. It
     *                  identifies the user and assignment to passback to.
     * @param score the score of the submission
     * @param fetchURL the url to send back to canvas (e.g., the codecheck report URL)
     */
    public String getScoreAndURLPassbackXML(String sourcedId, double score, String fetchURL)
    {
	return "<?xml version = \"1.0\" encoding = \"UTF-8\"?>\n"
	    + "<imsx_POXEnvelopeRequest xmlns = \"http://www.imsglobal.org/lis/oms1p0/pox\">\n"
	    + "  <imsx_POXHeader>\n"
	    + "    <imsx_POXRequestHeaderInfo>\n"
	    + "      <imsx_version>V1.0</imsx_version>\n"
	    + "      <imsx_messageIdentifier>12341234</imsx_messageIdentifier>\n"
	    + "    </imsx_POXRequestHeaderInfo>\n"
	    + "  </imsx_POXHeader>\n"
	    + "  <imsx_POXBody>\n"
	    + "    <replaceResultRequest>\n"
	    + "      <resultRecord>\n"
	    + "        <sourcedGUID>\n"
	    + "          <sourcedId>" + sourcedId + "</sourcedId>\n"
	    + "        </sourcedGUID>\n"
	    + "        <result>\n"
	    + "          <resultScore>\n"
	    + "            <language>en</language>\n"
	    + "            <textString>" + score + "</textString>\n"
	    + "          </resultScore>\n"
	    + "          <!-- Added element -->\n" // This element sends the report URL back to Canvas
	    + "          <resultData>\n"
	    + "          <url>" + fetchURL + "</url>\n"
	    + "          </resultData>\n"
	    + "        </result>\n"
	    + "      </resultRecord>\n"
	    + "    </replaceResultRequest>\n"
	    + "  </imsx_POXBody>\n"
	    + "</imsx_POXEnvelopeRequest>\n";
    }

    /**
     * Pass back the grade to Canvas. If the <code>xml</code> is set
     * up with fetch URL string, then also pass back the URL to the
     * codecheck report.
     * @param gradePassbackURL the grade passback URL from the LTI launch
     * @param xml the data to send off, with the sourcedId, score, and possibly the fetchURL
     * @param keyOauth the oauth consumer key
     * @param secretOauth the oauth secret key
     */
    public URLConnection passbackGradeToCanvas(String gradePassbackURL, String xml,
					       String keyOauth, String secretOauth)
	throws URISyntaxException, MalformedURLException, IOException, UnsupportedEncodingException,
	       OAuthMessageSignerException, OAuthExpectationFailedException, OAuthCommunicationException
    {
    	// now post the score to canvas. Make sure to sign the POST correctly
    	// with OAuth 1.0, including the digest of the XML body. Also make sure
    	// to set the content-type to application/xml.

	// Create an oauth consumer in order to sign the grade that will be sent.
    	DefaultOAuthConsumer consumer = new DefaultOAuthConsumer(keyOauth, secretOauth);

    	// empty token key and token secret, because they are not
    	// needed to sign the request that will pass back the grade.
    	// This is how the Ruby OAuth code works too.
    	consumer.setTokenWithSecret("", "");

    	// This is the URL that we will send the grade to so it can go back to Canvas
    	URL url = new URL(gradePassbackURL);

    	// This is the part where we send the HTTP request
    	HttpURLConnection request = (HttpURLConnection) url.openConnection();
	
    	 // set http request to POST
    	request.setRequestMethod("POST");
    	request.setDoOutput(true);

    	// set the content type to accept xml
    	request.setRequestProperty("Content-Type", "application/xml");

    	// set the content-length to be the length of the xml
    	request.setRequestProperty("Content-Length", Integer.toString(xml.length()));



	// Sign the request per the oauth 1.0 spec
	consumer.sign(request); // Throws
				// OAuthMessageSignerException,
				// OAuthExpectationFailedException,
				// OAuthCommunicationException
	
    	// POST the xml to the grade passback url
    	request.getOutputStream().write(xml.getBytes("UTF8"));

    	// send the request
    	request.connect();

	return request;
    }

    /**
       Gets the response body of the http request
       @param request the request object to get the response of.
       @return the response body
     */
    public String getResponseBody(URLConnection request) throws IOException
    {
	StringBuilder response = new StringBuilder();
    	try (Scanner in = new Scanner(request.getInputStream())) {
		while (in.hasNextLine()) {
    		    response.append(in.nextLine());
    		    response.append("\n");
    		}
    	    }
    	catch (IOException e) {
    	    if (!(request instanceof HttpURLConnection)) throw e;
    	    InputStream err = ((HttpURLConnection) request).getErrorStream();
    	    if (err == null) throw e;
    	    Scanner in = new Scanner(err);
    	    response.append(in.nextLine());
    	    response.append("\n");
    	}
	
	return response.toString();
    }

    /**
       Converts the fractional score (in the form "0" or "x/y", as a String)
       to be a double in the range [0.0,1.0].

       The codecheck fractional score will always be valued in the
       range [0,1] (no extra credit points are given).
       @param score the score to parse
       @return the normalized fraction
     */
    public static double normalizeScore(String score) {
	// If the score is the zero score ("0"), then return a 0.
	if (score.equals("0")) {
	    return 0;
	}

	// parse the string, which is a fraction delimited by a slash
	int indexDelimiter = score.indexOf("/");
	double numerator = Double.parseDouble(score.substring(0, indexDelimiter));
	double denominator = Double.parseDouble(score.substring(indexDelimiter + 1));

	return numerator / denominator;
    }

    @GET
    @Path("/xml-config")
    @Produces("application/xml")
    public Response xmlConfig()
    {
    	String hostport = request.getServerName() + ":" + request.getServerPort(); // e.g., localhost:8080
    	String contextPath = context.getContextPath(); // e.g., /lti-interactive
    	String output = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
    	    + "<cartridge_basiclti_link xmlns=\"http://www.imsglobal.org/xsd/imslticc_v1p0\"\n"
    	    + "    xmlns:blti = \"http://www.imsglobal.org/xsd/imsbasiclti_v1p0\"\n"
    	    + "    xmlns:lticm =\"http://www.imsglobal.org/xsd/imslticm_v1p0\"\n"
    	    + "    xmlns:lticp =\"http://www.imsglobal.org/xsd/imslticp_v1p0\"\n"
    	    + "    xmlns:xsi = \"http://www.w3.org/2001/XMLSchema-instance\"\n"
    	    + "    xsi:schemaLocation = \"http://www.imsglobal.org/xsd/imslticc_v1p0 http://www.imsglobal.org/xsd/lti/ltiv1p0/imslticc_v1p0.xsd\n"
    	    + "    http://www.imsglobal.org/xsd/imsbasiclti_v1p0 http://www.imsglobal.org/xsd/lti/ltiv1p0/imsbasiclti_v1p0.xsd\n"
    	    + "    http://www.imsglobal.org/xsd/imslticm_v1p0 http://www.imsglobal.org/xsd/lti/ltiv1p0/imslticm_v1p0.xsd\n"
    	    + "    http://www.imsglobal.org/xsd/imslticp_v1p0 http://www.imsglobal.org/xsd/lti/ltiv1p0/imslticp_v1p0.xsd\">\n"
    	    + "    <blti:title>Interactive Exercises</blti:title>\n"
    	    + "    <blti:description>Select a codecheck problem to set as an assignment that will automatically send the score and the report URL back to the Canvas gradebook.</blti:description>\n"
    	    + "    <blti:launch_url>http://" + hostport + contextPath + "/exercises</blti:launch_url>\n"
    	    + "    <blti:extensions platform=\"canvas.instructure.com\">\n"
    	    + "      <lticm:property name=\"privacy_level\">public</lticm:property>\n"
    	    + "      <lticm:property name=\"domain\">http://" + hostport + "</lticm:property>\n"
    	    + "      <lticm:options name=\"resource_selection\">\n"
    	    + "        <lticm:property name=\"enabled\">true</lticm:property>\n"
    	    + "        <lticm:property name=\"url\">http://" + hostport + contextPath + "/choose-problem</lticm:property>\n"
    	    + "        <lticm:property name=\"text\">Exercise Chooser</lticm:property>\n"
    	    + "        <lticm:property name=\"selection_width\">800</lticm:property>\n"
    	    + "        <lticm:property name=\"selection_height\">600</lticm:property>\n"
    	    + "      </lticm:options>\n"
    	    + "    </blti:extensions>\n"
    	    + "</cartridge_basiclti_link>\n";

    	return Response.status(200).entity(output).build();
    }
}
