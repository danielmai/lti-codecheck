# Codecheck LTI Application

This is a LTI application that will have codecheck assignments show up
within Canvas as an External Tool assignment. Visually, the
application looks exactly the same as normal codecheck. What this
application adds is automatic scoring into the gradebook when a
student submits to the assignment.

For setting up and more information visit the [wiki page][].

## External libraries

The [jsoup library][jsoup] is used under the
[MIT License](http://jsoup.org/license). It's used to work with the
HTML documents to change the relative URLs that appear in the
codecheck form.

The [oauth-signpost library][signpost] is used under the
[Apache license](http://www.apache.org/licenses/LICENSE-2.0). It is
used to sign the request that sends the score from the LTI tool back
to Canvas.

[wiki page]: https://bitbucket.org/danielmai/lti-codecheck/wiki/Home
[jsoup]: http://jsoup.org/
[signpost]: https://code.google.com/p/oauth-signpost/
